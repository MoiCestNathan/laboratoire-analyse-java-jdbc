module com.example.demo99 {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;

    opens com to javafx.fxml;
    opens com.controller to javafx.fxml;
    opens com.model to javafx.fxml, javafx.base;

    exports com.controller;
    exports com;
}
