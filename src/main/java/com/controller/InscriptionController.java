package com.controller;

import com.model.DatabaseConnection;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class InscriptionController {

    @FXML
    private TextField txtNom;

    @FXML
    private TextField txtPrenom;

    @FXML
    private TextField txtNumeroSecu;

    @FXML
    private TextField txtMotDePasse;

    @FXML
    private Button btnAnnuler;


    @FXML
    private void handleInscription() {
        String nom = txtNom.getText();
        String prenom = txtPrenom.getText();
        String numeroSecu = txtNumeroSecu.getText();
        String motDePasse = txtMotDePasse.getText();

        if (userExists(numeroSecu)) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur d'Inscription");
            alert.setHeaderText(null);
            alert.setContentText("Un utilisateur avec ce numéro de sécurité sociale existe déjà !");
            alert.showAndWait();
        } else {
            try (Connection conn = DatabaseConnection.getConnection();
                 PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Utilisateur (nom, prenom, numeroSecu,motDePasse) VALUES (?, ?, ?, ?)")) {
                pstmt.setString(1, nom);
                pstmt.setString(2, prenom);
                pstmt.setString(3, numeroSecu);
                pstmt.setString(4, motDePasse);

                pstmt.executeUpdate();

                Alert successAlert = new Alert(Alert.AlertType.INFORMATION);
                successAlert.setTitle("Inscription Réussie");
                successAlert.setHeaderText(null);
                successAlert.setContentText("L'utilisateur a été ajouté avec succès !");
                successAlert.showAndWait();

                Stage stage = (Stage) txtNom.getScene().getWindow();
                Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/com/menu-principal.fxml")));
                scene.getStylesheets().add(getClass().getResource("/com/styles.css").toExternalForm());
                stage.setScene(scene);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private boolean userExists(String numeroSecu) {
        String sql = "SELECT COUNT(*) FROM Utilisateur WHERE numeroSecu = ?";
        try (Connection conn = DatabaseConnection.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, numeroSecu);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @FXML
    private void handleAnnuler(){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/menu-principal.fxml"));
            Stage stage = (Stage) btnAnnuler.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            scene.getStylesheets().add(getClass().getResource("/com/styles.css").toExternalForm());
            stage.setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
