package com.controller;

import com.LaboAnalyseApplication;
import com.model.SessionUtilisateur;
import com.model.Utilisateur;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;

public class MenuPrincipalController {

    @FXML
    private VBox rootVBox;

    @FXML
    private Button btnConnexion;

    @FXML
    private Button btnInscription;

    @FXML
    private Button btnReserverVisite;

    @FXML
    private Button btnVoirReservations;

    @FXML
    private  Button btnInscriptionMedecin;

    @FXML
    private Button btnDeconnecter;

    @FXML
    private Label lblNomUtilisateur;


    @FXML
    public void initialize() {
        updateUI();
    }

    @FXML
    private void handleConnexion() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(LaboAnalyseApplication.class.getResource("/com/connexion.fxml"));
            Scene scene = new Scene(fxmlLoader.load());

            Stage stage = (Stage) btnInscription.getScene().getWindow();
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }    }

    @FXML
    private void handleInscription() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(LaboAnalyseApplication.class.getResource("/com/inscription.fxml"));
            Scene scene = new Scene(fxmlLoader.load());

            Stage stage = (Stage) btnInscription.getScene().getWindow();
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleReserverVisite() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(LaboAnalyseApplication.class.getResource("/com/reservation.fxml"));
            Scene scene = new Scene(fxmlLoader.load());

            Stage stage = (Stage) btnInscription.getScene().getWindow();
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleVoirReservations() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(LaboAnalyseApplication.class.getResource("/com/voirreservation.fxml"));
            Scene scene = new Scene(fxmlLoader.load());

            Stage stage = (Stage) btnInscription.getScene().getWindow();
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void handleDeconnexion() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Déconnexion");
        alert.setHeaderText("Vous allez être déconnecté");
        alert.setContentText("Êtes-vous sûr de vouloir vous déconnecter ?");

        ButtonType buttonTypeValider = new ButtonType("Valider");
        ButtonType buttonTypeAnnuler = new ButtonType("Annuler", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeValider, buttonTypeAnnuler);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == buttonTypeValider) {
            SessionUtilisateur.getInstance().clear();
            initialize();

        } else {
            try {
                initialize();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/menu-principal.fxml"));
                Stage stage = (Stage) btnDeconnecter.getScene().getWindow();
                Scene scene = new Scene(loader.load());
                scene.getStylesheets().add(getClass().getResource("/com/styles.css").toExternalForm());
                stage.setScene(scene);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void updateUI() {
        Utilisateur utilisateur = SessionUtilisateur.getInstance().getUtilisateurConnecte();
        if (utilisateur != null) {
            lblNomUtilisateur.setText("Connecté en tant que " + utilisateur.getNom() + " " + utilisateur.getPrenom());
        }else{
            lblNomUtilisateur.setText("");
        }
        btnDeconnecter.setVisible(SessionUtilisateur.getInstance().getUtilisateurConnecte() != null);
        btnReserverVisite.setVisible(SessionUtilisateur.getInstance().getUtilisateurConnecte() != null);
        btnVoirReservations.setVisible(SessionUtilisateur.getInstance().getUtilisateurConnecte() != null);


        btnInscription.setVisible(SessionUtilisateur.getInstance().getUtilisateurConnecte() == null);
        btnConnexion.setVisible(SessionUtilisateur.getInstance().getUtilisateurConnecte() == null);
    }

    @FXML
    private void handleInscriptionMedecin() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(LaboAnalyseApplication.class.getResource("/com/inscription-medecin.fxml"));
            Scene scene = new Scene(fxmlLoader.load());

            Stage stage = (Stage) btnInscriptionMedecin.getScene().getWindow();
            stage.setScene(scene);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

