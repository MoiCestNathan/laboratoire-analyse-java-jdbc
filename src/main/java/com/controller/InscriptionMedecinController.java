package com.controller;

import com.model.DatabaseConnection;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class InscriptionMedecinController {

    @FXML
    private DatePicker datePickerEmploiDuTemps;

    private List<PlageHoraire> plagesHorairesTemp = new ArrayList<>();

    @FXML
    private TableView<PlageHoraire> tableViewEmploiDuTemps;

    private ObservableList<PlageHoraire> plagesHorairesObservable = FXCollections.observableArrayList();

    @FXML
    private TableColumn<PlageHoraire, LocalDate> colJour;
    @FXML
    private TableColumn<PlageHoraire, String> colHeureDebut;
    @FXML
    private TableColumn<PlageHoraire, String> colHeureFin;

    @FXML
    private Button btnAnnuler;

    @FXML
    private TextField txtNomMedecin;

    @FXML
    private TextField txtPrenomMedecin;

    @FXML
    private TextField txtNumeroSecuMedecin;

    @FXML
    private TextField txtSalaireMedecin;

    @FXML
    private TextField txtMotDePasseMedecin;

    @FXML
    private RadioButton rbSpecialite1, rbSpecialite2, rbSpecialite3, rbSpecialite4, rbSpecialite5;

    @FXML
    private ComboBox<String> comboBoxPlageHoraire;

    public void initialize() {
        ToggleGroup specialiteGroup = new ToggleGroup();
        rbSpecialite1.setToggleGroup(specialiteGroup);
        rbSpecialite2.setToggleGroup(specialiteGroup);
        rbSpecialite3.setToggleGroup(specialiteGroup);
        rbSpecialite4.setToggleGroup(specialiteGroup);
        rbSpecialite5.setToggleGroup(specialiteGroup);

        comboBoxPlageHoraire.getItems().addAll(
                "08:00 - 09:00",
                "09:00 - 10:00",
                "10:00 - 11:00",
                "11:00 - 12:00",
                "14:00 - 15:00",
                "15:00 - 16:00",
                "16:00 - 17:00",
                "17:00 - 18:00");

        tableViewEmploiDuTemps.setItems(plagesHorairesObservable);
        colJour.setCellValueFactory(new PropertyValueFactory<>("date"));
        colHeureDebut.setCellValueFactory(new PropertyValueFactory<>("heureDebut"));
        colHeureFin.setCellValueFactory(new PropertyValueFactory<>("heureFin"));
    }


    @FXML
    private void handleEnregistrer() {
        String nom = txtNomMedecin.getText();
        String prenom = txtPrenomMedecin.getText();
        String numeroSecu = txtNumeroSecuMedecin.getText();
        String motDePasse = txtMotDePasseMedecin.getText();

        String salaireTexte = txtSalaireMedecin.getText();
        Double salaire = null;
        salaire = Double.parseDouble(salaireTexte);

        String specialite = getSelectedSpecialite();

        if (userExists(numeroSecu)) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur d'Inscription");
            alert.setHeaderText(null);
            alert.setContentText("Un utilisateur avec ce numéro de sécurité sociale existe déjà !");
            alert.showAndWait();
        } else {
            try (Connection conn = DatabaseConnection.getConnection()) {
                // Insérer d'abord dans la table Utilisateur
                try (PreparedStatement pstmt = conn.prepareStatement("INSERT INTO Utilisateur (nom, prenom, numeroSecu, motDePasse) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
                    pstmt.setString(1, nom);
                    pstmt.setString(2, prenom);
                    pstmt.setString(3, numeroSecu);
                    pstmt.setString(4, motDePasse);
                    int affectedRows = pstmt.executeUpdate();

                    if (affectedRows > 0) {
                        try (ResultSet generatedKeys = pstmt.getGeneratedKeys()) {
                            if (generatedKeys.next()) {
                                int idUtilisateur = generatedKeys.getInt(1);

                                // Maintenant, insérer dans la table Medecin
                                try (PreparedStatement pstmtMedecin = conn.prepareStatement("INSERT INTO Medecin (specialite, salaire, idUtilisateur) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
                                    pstmtMedecin.setString(1, specialite);
                                    pstmtMedecin.setDouble(2, salaire);
                                    pstmtMedecin.setInt(3, idUtilisateur);
                                    pstmtMedecin.executeUpdate();

                                    int idMedecin = -1;
                                    try (ResultSet rsMedecin = pstmtMedecin.getGeneratedKeys()) {
                                        if (rsMedecin.next()) {
                                            idMedecin = rsMedecin.getInt(1);
                                        }
                                    }

                                    // Vérifier si un médecin a été créé
                                    if (idMedecin != -1) {
                                        // Parcourir chaque plage horaire temporaire et l'insérer dans la base de données
                                        for (PlageHoraire plageHoraire : plagesHorairesTemp) {
                                            try (PreparedStatement pstmtEmploiDuTemps = conn.prepareStatement("INSERT INTO EmploiDuTempsMedecin (idMedecin, date, heureDebut, heureFin) VALUES (?, ?, ?, ?)")) {
                                                pstmtEmploiDuTemps.setInt(1, idMedecin);
                                                pstmtEmploiDuTemps.setDate(2, java.sql.Date.valueOf(plageHoraire.getDate()));
                                                pstmtEmploiDuTemps.setString(3, plageHoraire.getHeureDebut());
                                                pstmtEmploiDuTemps.setString(4, plageHoraire.getHeureFin());
                                                pstmtEmploiDuTemps.executeUpdate();
                                            }
                                        }
                                    }

                                }


                            }
                        }
                    }
                }
                Alert successAlert = new Alert(Alert.AlertType.INFORMATION);
                successAlert.setTitle("Inscription Réussie");
                successAlert.setHeaderText(null);
                successAlert.setContentText("L'utilisateur a été ajouté avec succès !");
                successAlert.showAndWait();

                Stage stage = (Stage) txtNomMedecin.getScene().getWindow();
                Scene scene = new Scene(FXMLLoader.load(getClass().getResource("/com/menu-principal.fxml")));
                scene.getStylesheets().add(getClass().getResource("/com/styles.css").toExternalForm());
                stage.setScene(scene);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private String getSelectedSpecialite() {
        if (rbSpecialite1.isSelected()) return "Cardiologie";
        if (rbSpecialite2.isSelected()) return "Dermatologie";
        if (rbSpecialite3.isSelected()) return "Neurologie";
        if (rbSpecialite4.isSelected()) return "Pédiatrie";
        if (rbSpecialite5.isSelected()) return "Radiologie";
        return null;
    }



    @FXML
    private void handleAnnuler(){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/menu-principal.fxml"));
            Stage stage = (Stage) btnAnnuler.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            scene.getStylesheets().add(getClass().getResource("/com/styles.css").toExternalForm());
            stage.setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean userExists(String numeroSecu) {
        String sql = "SELECT COUNT(*) FROM Utilisateur WHERE numeroSecu = ?";
        try (Connection conn = DatabaseConnection.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, numeroSecu);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @FXML
    private void handleAjouterPlageHoraire() {
        String plageHoraireSelectionnee = comboBoxPlageHoraire.getValue();
        LocalDate dateSelectionnee = datePickerEmploiDuTemps.getValue();

        if (plageHoraireSelectionnee != null && dateSelectionnee != null) {
            // Créez une nouvelle instance de PlageHoraire et ajoutez-la à la liste temporaire
            PlageHoraire nouvellePlage = new PlageHoraire(dateSelectionnee, plageHoraireSelectionnee);
            plagesHorairesTemp.add(nouvellePlage);
            plagesHorairesObservable.add(nouvellePlage);

            plagesHorairesObservable.sort(comparator);

            Alert successAlert = new Alert(Alert.AlertType.INFORMATION);
            successAlert.setTitle("Ajout Réussi");
            successAlert.setHeaderText(null);
            successAlert.setContentText("La plage horaire a été ajoutée avec succès.");
            successAlert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setHeaderText("Sélection incomplète");
            alert.setContentText("Veuillez sélectionner une date et une plage horaire.");
            alert.showAndWait();
        }
    }

    // Classe interne pour représenter une plage horaire
    public static class PlageHoraire {
        private LocalDate date;
        private String plageHoraire;

        public PlageHoraire(LocalDate date, String plageHoraire) {
            this.date = date;
            this.plageHoraire = plageHoraire;
        }

        public LocalDate getDate() {
            return date;
        }

        public void setDate(LocalDate date) {
            this.date = date;
        }

        public String getHeureDebut() {

            return plageHoraire.split(" - ")[0];
        }

        public String getHeureFin() {
            return plageHoraire.split(" - ")[1];
        }
    }




    Comparator<PlageHoraire> comparator = Comparator
            .comparing(PlageHoraire::getDate)
            .thenComparing(PlageHoraire::getHeureDebut);
}
