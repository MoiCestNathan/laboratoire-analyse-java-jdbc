package com.controller;

import com.model.DatabaseConnection;
import com.model.SessionUtilisateur;
import com.model.Utilisateur;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;

public class ReservationController {

    @FXML
    private TextField txtNom, txtPrenom, txtNumSecu;
    @FXML
    private ComboBox<String> cbTypeAnalyse;
    @FXML
    private Button btnReserver, btnAnnuler;

    int idUtilisateur;

    public void initialize() {
        Utilisateur utilisateurConnecte = SessionUtilisateur.getInstance().getUtilisateurConnecte();
        if (utilisateurConnecte != null) {
            txtNom.setText(utilisateurConnecte.getNom());
            txtPrenom.setText(utilisateurConnecte.getPrenom());
            txtNumSecu.setText(utilisateurConnecte.getNumeroSecu());
            idUtilisateur = getIdUtilisateurParNumSecu(utilisateurConnecte.getNumeroSecu());
        }
        chargerTypesAnalyse();
    }

    @FXML
    private void handleReservation() {
        String typeAnalyse = cbTypeAnalyse.getValue();
        if (typeAnalyse == null || typeAnalyse.isEmpty()) {
            // Afficher une alerte si aucun type d'analyse n'est sélectionné
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Aucune sélection");
            alert.setHeaderText(null);
            alert.setContentText("Veuillez sélectionner un type d'analyse.");
            alert.showAndWait();
            return;
        }

        try (Connection conn = DatabaseConnection.getConnection()) {
            // Requête pour trouver le premier créneau disponible pour le type d'analyse sélectionné
            String sql = "SELECT em.*, me.specialite FROM EmploiDuTempsMedecin em " +
                    "INNER JOIN Medecin me ON em.idMedecin = me.idMedecin " +
                    "WHERE me.specialite = ? AND em.reserve = 0 " +
                    "ORDER BY em.date, em.heureDebut " +
                    "LIMIT 1;";

            try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
                pstmt.setString(1, typeAnalyse);
                try (ResultSet rs = pstmt.executeQuery()) {
                    if (rs.next()) {
                        // Récupérer les informations du premier créneau disponible
                        Utilisateur utilisateurConnecte = SessionUtilisateur.getInstance().getUtilisateurConnecte();
                        int idEmploiDuTemps = rs.getInt("idEmploiDuTemps");
                        LocalDate date = rs.getDate("date").toLocalDate();
                        String heureDebut = rs.getString("heureDebut");
                        String heureFin = rs.getString("heureFin");

                        // Afficher une boîte de dialogue avec les informations du rendez-vous
                        Alert confirmationAlert = new Alert(Alert.AlertType.CONFIRMATION);
                        confirmationAlert.setTitle("Confirmation de réservation");
                        confirmationAlert.setHeaderText("Rendez-vous disponible");
                        confirmationAlert.setContentText(String.format("Votre rendez-vous est prévu le %s de %s à %s pour une %s.",
                                date.toString(), heureDebut, heureFin, typeAnalyse));

                        // Ajouter des boutons pour confirmer ou annuler la réservation
                        ButtonType buttonConfirmer = new ButtonType("Confirmer");
                        ButtonType buttonAnnuler = new ButtonType("Annuler", ButtonBar.ButtonData.CANCEL_CLOSE);
                        confirmationAlert.getButtonTypes().setAll(buttonConfirmer, buttonAnnuler);

                        Optional<ButtonType> result = confirmationAlert.showAndWait();
                        if (result.isPresent() && result.get() == buttonConfirmer) {
                            // Passer à la fenêtre de paiement
                            afficherPaiement(idEmploiDuTemps, idUtilisateur, typeAnalyse, date, heureDebut, heureFin);
                        }
                    } else {
                        // Aucun créneau disponible trouvé
                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Aucun créneau disponible");
                        alert.setHeaderText(null);
                        alert.setContentText("Il n'y a actuellement aucun créneau disponible pour ce type d'analyse.");
                        alert.showAndWait();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Afficher une alerte en cas d'erreur
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur");
            alert.setHeaderText("Problème lors de la recherche de créneau");
            alert.setContentText("Une erreur est survenue lors de la recherche du premier créneau disponible.");
            alert.showAndWait();
        }
    }


    @FXML
    private void handleAnnuler(){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/menu-principal.fxml"));
            Stage stage = (Stage) btnAnnuler.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            scene.getStylesheets().add(getClass().getResource("/com/styles.css").toExternalForm());
            stage.setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void chargerTypesAnalyse() {
        String sql = "SELECT nomAnalyse FROM Analyse";
        try (Connection conn = DatabaseConnection.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(sql);
             ResultSet rs = pstmt.executeQuery()) {
            while (rs.next()) {
                String nomAnalyse = rs.getString("nomAnalyse");
                cbTypeAnalyse.getItems().add(nomAnalyse);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void afficherPaiement(int idEmploiDuTemps, int idUtilisateur, String typeAnalyse, LocalDate date, String heureDebut, String heureFin) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/paiement.fxml"));
            Stage stage = (Stage) btnReserver.getScene().getWindow();
            Scene scene = new Scene(loader.load());

            PaiementController paiementController = loader.getController();
            paiementController.initialiserDetails(idEmploiDuTemps, idUtilisateur, typeAnalyse, date, heureDebut, heureFin);

            stage.setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getIdUtilisateurParNumSecu(String numeroSecu) {
        int idUtilisateur = -1; // Valeur par défaut signifiant "non trouvé"
        String sql = "SELECT idUtilisateur FROM Utilisateur WHERE numeroSecu = ?";
        try (Connection conn = DatabaseConnection.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {

            pstmt.setString(1, numeroSecu);
            ResultSet rs = pstmt.executeQuery();

            if (rs.next()) {
                idUtilisateur = rs.getInt("idUtilisateur");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idUtilisateur;
    }

}
