package com.controller;

import com.LaboAnalyseApplication;
import com.model.DatabaseConnection;
import com.model.Reservation;
import com.model.SessionUtilisateur;
import com.model.Utilisateur;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class VoirReservationController {
    @FXML
    private TableView<Reservation> tableReservations;
    @FXML
    private TableColumn<Reservation, LocalDate> colDate;
    @FXML
    private TableColumn<Reservation, String> colHeureDebut;
    @FXML
    private TableColumn<Reservation, String> colHeureFin;
    @FXML
    private TableColumn<Reservation, String> colTypeAnalyse;

    @FXML
    private Button btnMenu;

    @FXML
    public void initialize() {
        colDate.setCellValueFactory(new PropertyValueFactory<>("dateReservation"));
        colHeureDebut.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getHeureDebut().toString()));
        colHeureFin.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getHeureFin().toString()));
        colTypeAnalyse.setCellValueFactory(cellData -> new SimpleStringProperty(getNomAnalyse(cellData.getValue().getIdAnalyse())));

        chargerReservations();
    }

    private void chargerReservations() {
        Utilisateur utilisateurConnecte = SessionUtilisateur.getInstance().getUtilisateurConnecte();
        if (utilisateurConnecte != null) {
            try (Connection conn = DatabaseConnection.getConnection()) {
                // Première requête pour obtenir l'idUtilisateur basé sur le numeroSecu
                int idUtilisateur = -1;
                String sqlUtilisateur = "SELECT idUtilisateur FROM Utilisateur WHERE numeroSecu = ?";
                try (PreparedStatement pstmt = conn.prepareStatement(sqlUtilisateur)) {
                    pstmt.setString(1, utilisateurConnecte.getNumeroSecu());
                    ResultSet rsUser = pstmt.executeQuery();
                    if (rsUser.next()) {
                        idUtilisateur = rsUser.getInt("idUtilisateur");
                    }
                }

                // Vérifier si un idUtilisateur valide a été trouvé
                if (idUtilisateur != -1) {
                    // Seconde requête pour obtenir les réservations de l'utilisateur
                    String sqlReservations = "SELECT * FROM Reservation WHERE idUtilisateur = ?";
                    try (PreparedStatement pstmt = conn.prepareStatement(sqlReservations)) {
                        pstmt.setInt(1, idUtilisateur);
                        ResultSet rsReservations = pstmt.executeQuery();
                        while (rsReservations.next()) {
                            Reservation reservation = new Reservation();
                            reservation.setIdReservation(rsReservations.getInt("idReservation"));
                            reservation.setDateReservation(rsReservations.getDate("dateReservation").toLocalDate());
                            reservation.setHeureDebut(rsReservations.getTime("heureDebut").toLocalTime());
                            reservation.setHeureFin(rsReservations.getTime("heureFin").toLocalTime());
                            reservation.setIdUtilisateur(rsReservations.getInt("idUtilisateur"));
                            reservation.setIdAnalyse(rsReservations.getInt("idAnalyse"));
                            tableReservations.getItems().add(reservation);
                        }
                    }
                } else {
                    // Gérer le cas où l'utilisateur n'est pas trouvé ou une autre erreur
                    System.out.println("Aucun utilisateur correspondant trouvé.");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    private String getNomAnalyse(int idAnalyse) {
        // Récupérer le nom de l'analyse à partir de l'ID
        try (Connection conn = DatabaseConnection.getConnection()) {
            String sql = "SELECT nomAnalyse FROM Analyse WHERE idAnalyse = ?";
            try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
                pstmt.setInt(1, idAnalyse);
                ResultSet rs = pstmt.executeQuery();
                if (rs.next()) {
                    return rs.getString("nomAnalyse");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "Inconnu";
    }

    @FXML
    private void handleRevenir() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/menu-principal.fxml"));
            Stage stage = (Stage) btnMenu.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            scene.getStylesheets().add(getClass().getResource("/com/styles.css").toExternalForm());
            stage.setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

