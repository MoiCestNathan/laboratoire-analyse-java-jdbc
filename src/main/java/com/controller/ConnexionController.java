package com.controller;

import com.model.DatabaseConnection;
import com.model.SessionUtilisateur;
import com.model.Utilisateur;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class ConnexionController {

    private String utilisateurNom;
    private String utilisateurPrenom;
    private String utilisateurSecu;

    @FXML
    private TextField txtNumeroSecu;

    @FXML
    private PasswordField txtMotDePasse;

    @FXML
    private Button btnAnnuler;

    private boolean validateLogin(String numeroSecu, String motDePasse) {
        String sql = "SELECT COUNT(*) FROM Utilisateur WHERE numeroSecu = ? AND motDePasse = ?";
        try (Connection conn = DatabaseConnection.getConnection();
             PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, numeroSecu);
            pstmt.setString(2, motDePasse);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @FXML
    private void handleConnexion() {
        String numeroSecu = txtNumeroSecu.getText();
        String motDePasse = txtMotDePasse.getText();

        if (validateLogin(numeroSecu, motDePasse)) {
            try (Connection conn = DatabaseConnection.getConnection();
                 PreparedStatement pstmt = conn.prepareStatement("SELECT nom, prenom FROM Utilisateur WHERE numeroSecu = ?")) {
                pstmt.setString(1, numeroSecu);
                ResultSet rs = pstmt.executeQuery();
                if (rs.next()) {
                    String nom = rs.getString("nom");
                    String prenom = rs.getString("prenom");

                    Utilisateur utilisateur = new Utilisateur(nom, prenom, numeroSecu);
                    SessionUtilisateur.getInstance().setUtilisateurConnecte(utilisateur);

                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Connexion Réussie");
                    alert.setHeaderText(null);
                    alert.setContentText("Connecté en tant que " + nom + " " + prenom + " " + numeroSecu);
                    alert.showAndWait();

                    try {
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/menu-principal.fxml"));
                        Stage stage = (Stage) txtNumeroSecu.getScene().getWindow();
                        Scene scene = new Scene(loader.load());
                        scene.getStylesheets().add(getClass().getResource("/com/styles.css").toExternalForm());
                        stage.setScene(scene);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur de Connexion");
            alert.setHeaderText(null);
            alert.setContentText("Identifiant ou mot de passe incorrect !");
            alert.showAndWait();
        }
    }

    @FXML
    private void handleAnnuler() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/menu-principal.fxml"));
            Stage stage = (Stage) btnAnnuler.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            scene.getStylesheets().add(getClass().getResource("/com/styles.css").toExternalForm());
            stage.setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
