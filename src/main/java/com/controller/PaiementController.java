package com.controller;

import com.model.DatabaseConnection;
import com.model.SessionUtilisateur;
import com.model.Utilisateur;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.sql.*;
import java.time.LocalDate;
import java.util.Optional;

public class PaiementController {

    private int idEmploiDuTemps;
    private int idUtilisateur;
    private String typeAnalyse;
    private LocalDate date;
    private String heureDebut;
    private String heureFin;

    @FXML
    private TextField txtNomTitulaire, txtNumeroCarte, txtCodeSecurite;
    @FXML
    private ComboBox<String> cbMoisExpiration, cbAnneeExpiration;
    @FXML
    private Button btnValider, btnAnnuler;

    @FXML
    private void initialize() {
        // Initialiser le ComboBox pour les mois
        cbMoisExpiration.getItems().addAll(
                "01", "02", "03", "04", "05", "06",
                "07", "08", "09", "10", "11", "12"
        );

        // Initialiser le ComboBox pour les années
        int currentYear = java.time.Year.now().getValue();
        for (int i = 0; i < 10; i++) {
            cbAnneeExpiration.getItems().add(String.valueOf(currentYear + i));
        }
    }

    @FXML
    private void handleValider() {
        if (verifierContraintes()) {
            // Afficher une confirmation
            Alert confirmationAlert = new Alert(Alert.AlertType.CONFIRMATION);
            confirmationAlert.setTitle("Confirmation de paiement");
            confirmationAlert.setHeaderText("Vous êtes sur le point de valider la transaction");
            confirmationAlert.setContentText("Êtes-vous sûr ?");
            Optional<ButtonType> result = confirmationAlert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {

                // Mettre à jour la plage horaire comme réservée dans la base de données
                marquerPlageHoraireReservee(idEmploiDuTemps);
                enregistrerReservation();

                // Afficher une alerte de succès
                Alert successAlert = new Alert(Alert.AlertType.INFORMATION);
                successAlert.setTitle("Paiement réussi");
                successAlert.setHeaderText(null);
                successAlert.setContentText("Votre paiement a été effectué avec succès, réservation du créneau effectuée.");
                successAlert.showAndWait();

                // Rediriger vers l'accueil
                try {
                    FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/menu-principal.fxml"));
                    Stage stage = (Stage) txtNomTitulaire.getScene().getWindow();
                    Scene scene = new Scene(loader.load());
                    scene.getStylesheets().add(getClass().getResource("/com/styles.css").toExternalForm());
                    stage.setScene(scene);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean verifierContraintes() {
        // Vérifier le numéro de la carte
        if (txtNumeroCarte.getText().length() != 16 || !txtNumeroCarte.getText().matches("\\d{16}")) {
            afficherAlerte("Numéro de carte invalide", "Le numéro de la carte doit contenir exactement 16 chiffres.");
            return false;
        }

        // Vérifier le code de sécurité
        if (txtCodeSecurite.getText().length() != 3 || !txtCodeSecurite.getText().matches("\\d{3}")) {
            afficherAlerte("Code de sécurité invalide", "Le code de sécurité doit contenir exactement 3 chiffres.");
            return false;
        }

        // Vérifier que le mois et l'année d'expiration sont sélectionnés
        if (cbMoisExpiration.getValue() == null || cbAnneeExpiration.getValue() == null) {
            afficherAlerte("Date d'expiration invalide", "Veuillez sélectionner le mois et l'année d'expiration.");
            return false;
        }
        // Si toutes les vérifications sont passées
        return true;
    }

    @FXML
    private void handleAnnuler(){
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/com/menu-principal.fxml"));
            Stage stage = (Stage) btnAnnuler.getScene().getWindow();
            Scene scene = new Scene(loader.load());
            scene.getStylesheets().add(getClass().getResource("/com/styles.css").toExternalForm());
            stage.setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void afficherAlerte(String titre, String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(titre);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }

    public void initialiserDetails(int idEmploiDuTemps, int idUtilisateur, String typeAnalyse, LocalDate date, String heureDebut, String heureFin) {
        this.idEmploiDuTemps = idEmploiDuTemps;
        this.idUtilisateur = idUtilisateur;
        this.typeAnalyse = typeAnalyse;
        this.date = date;
        this.heureDebut = heureDebut;
        this.heureFin = heureFin;
    }

    private void marquerPlageHoraireReservee(int idEmploiDuTemps) {
        // Connexion à la base de données
        try (Connection conn = DatabaseConnection.getConnection()) {
            // Requête SQL pour mettre à jour la plage horaire comme réservée
            String sql = "UPDATE EmploiDuTempsMedecin SET reserve = 1 WHERE idEmploiDuTemps = ?";

            // Préparation de la requête
            try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
                // Définir les valeurs des paramètres
                pstmt.setInt(1, idEmploiDuTemps);

                // Exécuter la mise à jour
                int affectedRows = pstmt.executeUpdate();

                // Vérifier si la mise à jour a été effectuée
                if (affectedRows > 0) {
                    System.out.println("La plage horaire a été marquée comme réservée.");
                } else {
                    System.out.println("Aucune plage horaire mise à jour. Vérifiez l'ID.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            // Afficher une alerte ou gérer l'erreur comme vous le souhaitez
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Erreur de mise à jour");
            alert.setHeaderText("Problème lors de la mise à jour de la plage horaire");
            alert.setContentText("Une erreur est survenue lors de la tentative de marquer la plage horaire comme réservée.");
            alert.showAndWait();
        }
    }

    private void enregistrerReservation() {
        try (Connection conn = DatabaseConnection.getConnection()) {
            // Requête pour insérer une nouvelle réservation
            String sql = "INSERT INTO Reservation (dateReservation, heureDebut, heureFin, idUtilisateur, idAnalyse) VALUES (?, ?, ?, ?, ?)";
            try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
                // Convertir LocalDate et les chaînes d'heure en types SQL appropriés
                pstmt.setDate(1, Date.valueOf(date));
                pstmt.setTime(2, Time.valueOf(heureDebut));
                pstmt.setTime(3, Time.valueOf(heureFin));
                System.out.println(idUtilisateur);
                pstmt.setInt(4, idUtilisateur);
                pstmt.setInt(5, getIdAnalyse(typeAnalyse, conn));

                int affectedRows = pstmt.executeUpdate();
                if (affectedRows > 0) {
                    System.out.println("Réservation enregistrée avec succès.");
                } else {
                    System.out.println("Aucune réservation enregistrée.");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getIdAnalyse(String nomAnalyse, Connection conn) throws SQLException {
        // Requête pour obtenir l'ID d'une analyse à partir de son nom
        String sql = "SELECT idAnalyse FROM Analyse WHERE nomAnalyse = ?";
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, nomAnalyse);
            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt("idAnalyse");
                }
            }
        }
        System.out.println("Analyse invalide");
        return -1;
    }
}
