package com.model;



public class Utilisateur {
    private int idUtilisateur;
    private String nom;
    private String prenom;
    private String numeroSecu;

    private String motDePasse;

    public Utilisateur(){
    }

    public Utilisateur(String nom, String prenom, String numeroSecu) {
        this.nom = nom;
        this.prenom = prenom;
        this.numeroSecu = numeroSecu;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public void setIdUtilisateur(int idUtilisateur) {
        this.idUtilisateur = idUtilisateur;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNumeroSecu() {
        return numeroSecu;
    }

    public void setNumeroSecu(String numeroSecu) {
        this.numeroSecu = numeroSecu;
    }
}

