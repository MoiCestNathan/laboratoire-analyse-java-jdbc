package com.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnection {

    /**
     * Établit et retourne une connexion à la base de données.
     *
     * Les informations de connexion sont récupérées à partir de la classe databaseConfig.
     *
     * @return Connection - un objet Connection à la base de données.
     * @throws SQLException - en cas d'échec de la connexion.
     */
    public static Connection getConnection() throws SQLException {
        // Utilise DriverManager pour obtenir une connexion à la base de données
        // Les paramètres URL, USER et PASSWORD sont définis dans la classe databaseConfig
        return DriverManager.getConnection(DatabaseConfig.URL, DatabaseConfig.USER, DatabaseConfig.PASSWORD);
    }
}
