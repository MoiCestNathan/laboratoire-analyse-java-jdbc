package com.model;

public class Analyse {
    private int idAnalyse;
    private String nomAnalyse;
    private String description;

    public int getIdAnalyse() {
        return idAnalyse;
    }

    public void setIdAnalyse(int idAnalyse) {
        this.idAnalyse = idAnalyse;
    }

    public String getNomAnalyse() {
        return nomAnalyse;
    }

    public void setNomAnalyse(String nomAnalyse) {
        this.nomAnalyse = nomAnalyse;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
