package com.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DataInserter {

    /**
     * Insère des spécialités dans la table Analyse, si elles n'existent pas déjà.
     */
    public static void insererSpecialites() {
        // Les spécialités à insérer
        String[] specialites = {"Cardiologie", "Dermatologie", "Neurologie", "Pédiatrie", "Radiologie"};

        // Connexion à la base de données
        try (Connection conn = DatabaseConnection.getConnection()) {
            for (String specialite : specialites) {
                // Vérifier si la spécialité existe déjà
                if (!specialiteExiste(specialite, conn)) {
                    // Préparation de la requête SQL pour l'insertion
                    String sql = "INSERT INTO Analyse (nomAnalyse) VALUES (?)";
                    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
                        // Définir les valeurs des paramètres
                        pstmt.setString(1, specialite);
                        // Exécuter la requête
                        pstmt.executeUpdate();
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Vérifie si une spécialité existe déjà dans la base de données.
     */
    private static boolean specialiteExiste(String specialite, Connection conn) throws SQLException {
        String sql = "SELECT COUNT(*) FROM Analyse WHERE nomAnalyse = ?";
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, specialite);
            try (ResultSet rs = pstmt.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1) > 0;
                }
            }
        }
        return false;
    }
}
