package com.model;

import java.sql.Connection;
import java.sql.Statement;

public class DatabaseSetup {

    /**
     * Crée les tables nécessaires dans la base de données.
     */
    public static void createTables() {
        try (Connection connection = DatabaseConnection.getConnection();
             Statement statement = connection.createStatement()) {

            // Création de la table Utilisateur
            String createUtilisateurTable = "CREATE TABLE IF NOT EXISTS Utilisateur (" +
                    "idUtilisateur INT PRIMARY KEY AUTO_INCREMENT, " +
                    "nom VARCHAR(255), " +
                    "prenom VARCHAR(255), " +
                    "numeroSecu VARCHAR(255), " +
                    "motDePasse VARCHAR(255));";
            statement.executeUpdate(createUtilisateurTable);

            // Création de la table Médecin
            String createMedecinTable = "CREATE TABLE IF NOT EXISTS Medecin (" +
                    "idMedecin INT PRIMARY KEY AUTO_INCREMENT, " +
                    "specialite VARCHAR(255), " +
                    "salaire DECIMAL(10, 2), " +
                    "idUtilisateur INT, " +
                    "FOREIGN KEY (idUtilisateur) REFERENCES Utilisateur(idUtilisateur));";
            statement.executeUpdate(createMedecinTable);

            // Création de la table Analyse
            String createAnalyseTable = "CREATE TABLE IF NOT EXISTS Analyse (" +
                    "idAnalyse INT PRIMARY KEY AUTO_INCREMENT, " +
                    "nomAnalyse VARCHAR(255), " +
                    "description TEXT);";
            statement.executeUpdate(createAnalyseTable);

            // Création de la table Reservation
            String createReservationTable = "CREATE TABLE IF NOT EXISTS Reservation (" +
                    "idReservation INT PRIMARY KEY AUTO_INCREMENT, " +
                    "dateReservation DATE, " +
                    "heureDebut TIME, " +
                    "heureFin TIME, " +
                    "idUtilisateur INT, " +
                    "idAnalyse INT, " +
                    "FOREIGN KEY (idUtilisateur) REFERENCES Utilisateur(idUtilisateur), " +
                    "FOREIGN KEY (idAnalyse) REFERENCES Analyse(idAnalyse));";

            statement.executeUpdate(createReservationTable);

            // Création de la table Paiement
            String createPaiementTable = "CREATE TABLE IF NOT EXISTS Paiement (" +
                    "idPaiement INT PRIMARY KEY AUTO_INCREMENT, " +
                    "montant DECIMAL(10, 2), " +
                    "datePaiement DATETIME, " +
                    "idReservation INT, " +
                    "FOREIGN KEY (idReservation) REFERENCES Reservation(idReservation));";
            statement.executeUpdate(createPaiementTable);

            // Création de la table EmploiDuTempsMedecin
            String createEmploiDuTempsMedecinTable = "CREATE TABLE IF NOT EXISTS EmploiDuTempsMedecin (" +
                    "idEmploiDuTemps INT PRIMARY KEY AUTO_INCREMENT, " +
                    "idMedecin INT, " +
                    "date DATE, " +
                    "heureDebut TIME, " +
                    "heureFin TIME, " +
                    "reserve TINYINT DEFAULT 0, " +
                    "FOREIGN KEY (idMedecin) REFERENCES Medecin(idMedecin));";
            statement.executeUpdate(createEmploiDuTempsMedecinTable);

            System.out.println("Tables créées avec succès.");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
