package com.model;

public class SessionUtilisateur {
    private static SessionUtilisateur instance = new SessionUtilisateur();
    private Utilisateur utilisateurConnecte;

    private SessionUtilisateur() {}

    public static SessionUtilisateur getInstance() {
        return instance;
    }

    public void setUtilisateurConnecte(Utilisateur utilisateur) {
        this.utilisateurConnecte = utilisateur;
    }

    public Utilisateur getUtilisateurConnecte() {
        return utilisateurConnecte;
    }

    public void clear() {
        utilisateurConnecte = null;
    }
}

