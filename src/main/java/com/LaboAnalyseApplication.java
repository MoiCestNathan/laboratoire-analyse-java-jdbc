    package com;

    import com.model.DataInserter;
    import com.model.DatabaseSetup;
    import javafx.application.Application;
    import javafx.fxml.FXMLLoader;
    import javafx.scene.Scene;
    import javafx.stage.Stage;

    import java.io.IOException;

    public class LaboAnalyseApplication extends Application {
        @Override
        public void start(Stage stage) throws IOException {
            FXMLLoader fxmlLoader = new FXMLLoader(LaboAnalyseApplication.class.getResource("/com/menu-principal.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 400, 400);
            scene.getStylesheets().add(getClass().getResource("/com/styles.css").toExternalForm());
            stage.setTitle("Laboratoire d'analyse !");
            stage.setScene(scene);
            stage.show();
        }

        public static void main(String[] args) {
            //DatabaseSetup.createTables(); // Compiler une première fois puis mettre en commentaire
            //DataInserter.insererSpecialites(); // Compiler une première fois puis mettre en commentaire
            launch();
        }
    }