# Projet Système de Gestion de Laboratoire d'Analyse Médicale

Ce projet est une application de gestion pour un laboratoire d'analyse médicale. Il permet aux utilisateurs de s'inscrire, de se connecter, de réserver des analyses médicales, de consulter les plages horaires des médecins, et de payer pour les services.

## Fonctionnalités

- **Inscription et Connexion :** Les utilisateurs peuvent créer un compte et se connecter à l'application.
- **Gestion des Médecins :** Les administrateurs peuvent ajouter et gérer les médecins ainsi que leurs spécialités et emplois du temps.
- **Réservation :** Les utilisateurs peuvent réserver une analyse médicale en fonction des spécialités et de la disponibilité des médecins.
- **Paiement :** Après avoir choisi un créneau, les utilisateurs peuvent procéder au paiement fictif.
- **Consultation des Réservations :** Les utilisateurs peuvent voir leurs réservations passées et à venir.


## Prérequis

- **JavaFX** pour l'interface utilisateur.
- **MySQL** pour la gestion de la base de données.
- **Java** pour la logique de l'application.
- **JDBC**

## Configuration Requise

- JDK 11 ou plus récent.
- MySQL Server.
- Un IDE supportant Java et JavaFX, comme IntelliJ IDEA


### Base de Données

1. Créer une base de données MySQL:
    ```sql
    CREATE DATABASE nom_de_la_base;
    ```
    Remplacez `nom_de_la_base` par le nom souhaité pour votre base de données.

2. Exécuter les scripts SQL fournis pour créer les tables nécessaires. `DataInserter.java`

3. Configurer le fichier `DatabaseConnection.java` avec vos propres paramètres de connexion à la base de données:
    - URL de la base de données.
    - Nom d'utilisateur.
    - Mot de passe.

### Environnement de Développement

1. Ouvrez le projet dans votre IDE préféré.
2. Assurez-vous que le JDK correct est configuré.
3. Vérifiez que les bibliothèques JavaFX sont correctement liées au projet.
4. Vérifier que les dépendances avec le connecteur JDBC sont corrects.

## Lancement de l'Application

Exécutez le fichier `LaboAnalyseApplication.java` pour démarrer l'application. Assurez-vous que votre serveur MySQL est en cours d'exécution.
